#!/bin/bash

###########################################
# Script for generating experiments table #
# for iterative deepening and BFS         #
# Author: Laure Petrucci                  #
# Version: 3.1                            #
# Date: 2020-10-14                        #
###########################################

function usage {
	echo -e "\033[1;31musage\033[0m: $0 [-h | [-l] [-t timeout] -o table_filename [-S | -i input_models]]"
}

function help {
	echo -e ""
	usage
	echo -e "\nExecutes the experiments on all models. The result is written in the file specified with the \033[1m-o\033[0m option"
	echo -e "\n\033[1m-h\033[0m\t\t\tThis help"
	echo -e "\n\033[1m-l\033[0m\t\t\tExecute exploration with layers"
	echo -e "\n\033[1m-t timeout\033[0m\t\tUses a specified value for the timeout (in seconds) \033[4m[default: 120]\033[0m"
	echo -e "\n\033[1m-o table_filename\033[0m\tOutputs the results in a csv file (with separator ;) named \033[4mtable_filename\033[0m"
	echo -e "\n\033[1m-S\033[0m\t\t\tUses a subset of the models"
	echo -e "\n\033[1m-i input_models\033[0m\t\tOnly the models (separated by space) in \033[4minput_models\033[0m are used (e.g. -i 'model1 model2')"
	exit
}

function process_results {
	# find maximal depth
	echo -n `grep "depth actually" $one_result | tail -n 1 | cut -d: -f2 | sed -e 's/\[0m//'`' ;'>> $output_file
	# minimal depth at which a cycle was found
	min_depth=`grep "Minimum depth" $one_result | tail -n 1 | cut -d: -f2 | sed -e 's/\[0m//'`
	if [ -z "$min_depth" ]
	then echo -n " --- ; " >> $output_file
	else echo -n "$min_depth ; " >> $output_file
	fi
	# total number of cycles found
	echo -n `grep "cycles found" $one_result | grep Total | tail -n 1 | cut -d: -f2 | sed -e 's/\[0m//'`' ; '>> $output_file
	# time of computation
	in_time=`grep "exact" $one_result`
	if [ -z "$in_time" ]
	then echo -n "TO" >> $output_file
	else echo -n `grep "completed after" $one_result | tail -n 1 | sed -e 's/\[Cycle (NDFS)\] State space exploration completed after //' \
		| sed -e 's/ seconds.//' | sed -e 's/ second.//'  | sed -e 's/\[0m//'` >> $output_file
	fi
}

function process_BFS_results {
	# time of computation
	in_time=`grep "exact" $one_result`
	# find number of cycles
	cyclenum=`grep "Found a cycle" $one_result | wc -l`
	# different positionsin output text if no cycle was found
	if [ $cyclenum -eq 0 ]
	then pos=10
	else if [ -z "$in_time" ]
		then pos=10
		else pos=8
		fi
	fi
	# find maximal depth
	echo -n `grep "depth of" $one_result | cut -d' ' -f$pos | cut -d: -f1 | sed -e 's/\[0m//'`' ; ' >> $output_file
	# minimal depth at which a cycle was found
	if [ $cyclenum -eq 0 ]
	then echo -n "--- ; " >> $output_file
	else echo -n $((`grep -e Computing -e cycle $one_result | grep -n Found | head -1 | cut -d: -f1`-1))' ; ' >> $output_file
	fi
	# number of cycles
	echo -n $cyclenum' ; ' >> $output_file
	# time of computation
	if [ -z "$in_time" ]
	then echo -n "TO" >> $output_file
	else echo -n `grep "completed after" $one_result  | sed -e 's/\[Cycle\] Algorithm completed after //' \
		| sed -e 's/ seconds.//' | sed -e 's/ second.//'  | sed -e 's/\[0m//'` >> $output_file
	fi
}

# main part of the script
imitator="../assets/imitator"
imitator_options="-no-output-result"
# get the options
layers= # no layers by default
timeout=120 # 2 minutes by default
output_file=

tests_dir="../assets/testcases"
exp_dir="${tests_dir}/acceptingExamples"
input_files="BRP coffee \
		critical-region critical-region4 F3 F4 FDDI4 FischerAHV93 flipflop fmtv1A1-v2 \
		fmtv1A3-v2 JLR-TACAS13 \
		lynch lynch5 \
		Pipeline-KP12-2-3 Pipeline-KP12-2-5 Pipeline-KP12-3-3 \
		RCP Sched2.100.0 \
		Sched2.100.2 \
		Sched2.50.0 \
		Sched2.50.2 simop \
		spsmall tgcTogether2 \
		WFAS-BBLS15-det"
while getopts "lhd:s:t:o:Si:" opt; do
case $opt in
	l) layers="-layer" ;;
	h) help ;;
	t) timeout=$OPTARG ;;
	o) output_file=$OPTARG ;;
	S) input_files="critical-region critical-region4 F3 F4 FDDI4 FischerAHV93 flipflop fmtv1A1-v2 \
		lynch lynch5 \
		Pipeline-KP12-2-3 \
		RCP Sched2.100.0 \
		Sched2.50.0 \
		spsmall tgcTogether2" ;;
	i) input_files=$OPTARG ;;
esac
done

if [ -z "$output_file" ]
then usage
	exit
fi

extension=".${output_file##*.}"
one_result="`basename $output_file $extension`.tmp"
rm -f $one_result
rm -f $output_file

# print the command line that was used
echo $0 $* > $output_file
# table with iterative depth and BFS
echo ' ; Depth 5, step 5 ; ; ; ; Depth 5, step 5, -no-green ; ; ; ; Depth 10, step 5 ; ; ; ; Depth 10, step 10 ; ; ; ; Depth 0, step 1 ; ; ; ; BFS ; ; ;' >> $output_file
echo 'Model ; d ; m ; c ; t ; d ; m ; c ; t ; d ; m ; c ; t ; d ; m ; c ; t ; d ; m ; c ; t ; d ; m ; c ; t' >> $output_file
for f in $input_files
do echo -e "Running experiments for model \033[1;31m$f\033[0m"
	$imitator $imitator_options -mode checksyntax -verbose low $exp_dir/$f.imi > $one_result 2> /dev/null
	# first column = benchmark file name
	echo -n "$f ; " >> $output_file
	# columns 2 to 4 = L, X, P
	# echo -n `grep -e locations $one_result | cut -d, -f2,5,7 | sed -e 's/^ //' \
	# 	| sed -e 's/ locations, / \; /' | sed -e 's/ clock variables, / \; /' \
	# 	| sed -e 's/ parameters/ \; /' | sed -e 's/ parameter/ \; /' ` >> $output_file
# iterative deepening depth-init=5, depth-step=5
	echo -e "\tdepth-init=5, depth-step=5"
	$imitator $imitator_options  $layers -time-limit $timeout -depth-init 5 -depth-step 5 $exp_dir/$f.imi $exp_dir/accepting.imiprop > $one_result 2> /dev/null
	process_results
	echo -n ' ; ' >> $output_file
# iterative deepening depth-init=5, depth-step=5, no green
	echo -e "\tdepth-init=5, depth-step=5, no green"
	$imitator $imitator_options $layers -no-green -time-limit $timeout -depth-init 5 -depth-step 5 $exp_dir/$f.imi $exp_dir/accepting.imiprop > $one_result 2> /dev/null
	process_results
	echo -n ' ; ' >> $output_file
# iterative deepening depth-init=10, depth-step=5
	echo -e "\tdepth-init=10, depth-step=5"
	$imitator $imitator_options $layers -time-limit $timeout -depth-init 10 -depth-step 5 $exp_dir/$f.imi $exp_dir/accepting.imiprop > $one_result 2> /dev/null
	process_results
	echo -n ' ; ' >> $output_file
# iterative deepening depth-init=10, depth-step=10
	echo -e "\tdepth-init=10, depth-step=10"
	$imitator $imitator_options $layers -time-limit $timeout -depth-init 10 -depth-step 10 $exp_dir/$f.imi $exp_dir/accepting.imiprop > $one_result 2> /dev/null
	process_results
	echo -n ' ; ' >> $output_file
# iterative deepening depth-init=0, depth-step=1
	echo -e "\tdepth-init=0, depth-step=1"
	$imitator $imitator_options $layers -time-limit $timeout -depth-init 0 -depth-step 1 $exp_dir/$f.imi $exp_dir/accepting.imiprop > $one_result 2> /dev/null
	process_results
	echo -n ' ; ' >> $output_file
# BFS
	echo -e "\tBFS"
	$imitator $imitator_options -cycle-algo=BFS -time-limit $timeout $exp_dir/$f.imi $exp_dir/accepting.imiprop > $one_result 2> /dev/null
	process_BFS_results
	echo '' >> $output_file
done
rm -f $one_result
