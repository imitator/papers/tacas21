#!/bin/bash

##############################
# Script for BRP experiments #
# Author: Laure Petrucci     #
# Version: 2.1               #
# Date: 2020-10-14           #
##############################

function execute_imi {
	len="$((${#1} + 10))"
	echo -n -e "\e[7;49;33m" ; printf " %${len}s" ; echo -e "\e[0m"
	echo -e "\e[7;49;33m imitator $1 \e[0m"
	echo -n -e "\e[7;49;33m" ; printf " %${len}s" ; echo -e "\e[0m"
	imitator="../assets/imitator"
	$imitator -no-output-result $1
}

# parse options
function help {
  echo -e "Usage: $0 [-h] [-a]\n"
	echo -e "  -h\t\t\tDisplay this help message."
	echo -e "  -a\t\t\tRun also the experiment of section 6.1 with MAX=20."
}

# install deb packages
run_all=false
while getopts ":ha" opt; do
  case ${opt} in
    h)  help; exit 0;;
    a)  run_all=true;;
    \?) help; exit 1;;
  esac
done

tests_dir="../assets/testcases"
exp_dir="${tests_dir}/BRP"

# experiments of section 6.1
echo -e '\e[7;49;32m                              \e[0m'
echo -e '\e[7;49;32m  Experiments of section 6.1  \e[0m'
echo -e '\e[7;49;32m                              \e[0m'

execute_imi "-mergeq -comparison inclusion $exp_dir/brp_Channels.imi $exp_dir/brp_Channels.imiprop"
execute_imi "-mergeq -comparison inclusion $exp_dir/brp_RC.imi $exp_dir/brp_RC.imiprop"
execute_imi "-mergeq -comparison inclusion $exp_dir/brp_RC_MAX3.imi $exp_dir/brp_RC.imiprop"
execute_imi "-mergeq -comparison inclusion $exp_dir/brp_RC_MAX4.imi $exp_dir/brp_RC.imiprop"

if $run_all ; then
	execute_imi "-mergeq -comparison inclusion $exp_dir/brp_RC_MAX20.imi $exp_dir/brp_RC.imiprop"
fi

# experiments of section 6.2
echo -e '\e[7;49;32m                              \e[0m'
echo -e '\e[7;49;32m  Experiments of section 6.2  \e[0m'
echo -e '\e[7;49;32m                              \e[0m'
execute_imi "$exp_dir/brp_RC.imi $exp_dir/accepting.imiprop -depth-step=5 -depth-limit=25 -recompute-green"

# experiments of section 6.3
echo -e '\e[7;49;32m                              \e[0m'
echo -e '\e[7;49;32m  Experiments of section 6.3  \e[0m'
echo -e '\e[7;49;32m                              \e[0m'
execute_imi "-no-subsumption -comparison inclusion $exp_dir/brp_GF_S_in_RC.imi $exp_dir/accepting.imiprop"
execute_imi "-no-subsumption -comparison inclusion $exp_dir/brp_GF_S_in_RC2.imi $exp_dir/accepting.imiprop"
execute_imi "-no-subsumption -comparison inclusion $exp_dir/brp_GSinFSdk.imi $exp_dir/accepting.imiprop"
execute_imi "$exp_dir/brp_GSinFSnok.imi $exp_dir/accepting_one.imiprop"
